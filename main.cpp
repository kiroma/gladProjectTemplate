#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stdexcept>
#include <iostream>

int main()
{
	struct glfwGuard
	{
		~glfwGuard()
		{
			glfwTerminate();
		}
	}glfwGuard;
	glfwInit();
	glfwSetErrorCallback([](int errc, const char* errm){
		std::cout << "GLFW error (" << errc << "): " << errm << '\n';
	});
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	GLFWwindow* window = glfwCreateWindow(800, 600, "Window", nullptr, nullptr);
	if(!window)
	{
		throw std::runtime_error("Failed to create a GLFW window");
	}
	glfwMakeContextCurrent(window);
	if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		throw std::runtime_error("Failed to load OpenGL functions");
	}
	while(!glfwWindowShouldClose(window))
	{
		glfwWaitEvents();
	}
	glfwDestroyWindow(window);
}
